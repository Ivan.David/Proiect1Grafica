#pragma once
#include <vector>
#include <time.h>
#include "Punct.h"

class Triunghi{
private:
	Punct A, B, C;  //puncte initiale
	Punct A1, B1, C1; //puncte curente
	
	double genereazaNumarRandom(double min, double max);
	
	double arieTriunghi(bool initial);
public:
	double alpha = 0.9, beta = 0.9;
	double x, y;

	Triunghi();
	Triunghi(const Punct &A, const Punct &B, const Punct &C) { this->A = A; this->B = B; this->C = C;
																		//modificare parametrii viteza triunghiuri
																	this->alpha = genereazaNumarRandom(0.75, 1.4);
																	this->beta = genereazaNumarRandom(0.75, 1.4);
															 };
	Triunghi(const Punct *A, const Punct *B, const Punct *C) { this->A = *A; this->B = *B; this->C = *C; };
	~Triunghi() { };

	bool apartineTriunghi(Punct punct);
	
	bool seSuprapune(std::vector<Triunghi> triunghiVector);

	float getXMaxim();
	float getYMaxim();
	float getXMinim();
	float getYMinim();

	void desen();
	void transformari(bool suprapus);

	void updateFactoriTranslatare();
	void updatePozitie();
};