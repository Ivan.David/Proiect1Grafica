#include <GL/freeglut.h>

#include <cmath>
#include <random>

#include "Triunghi.h"
#include "constante.cpp"

double Triunghi::genereazaNumarRandom(double min, double max)
{
	std::random_device rand_dev;
	std::mt19937 generator(rand_dev());
	std::uniform_real_distribution<double>  distr(min, max);
	
	double numarRandom = distr(generator);

	return numarRandom;
}

double Triunghi::arieTriunghi(bool initial)
{
	double arie;

	if (!initial) {
		float xA = A1.getX(), yA = A1.getY();
		float xB = B1.getX(), yB = B1.getY();
		float xC = C1.getX(), yC = C1.getY();

		double result = xA*yB + xB*yC + yA*xC - yB*xC - yC*xA - yA*xB;

		arie = (1.0 / 2.0) * std::abs(result);
		
		return arie;
	} else {
		float xA = A.getX(), yA = A.getY();
		float xB = B.getX(), yB = B.getY();
		float xC = C.getX(), yC = C.getY();

		double result = xA*yB + xB*yC + yA*xC - yB*xC - yC*xA - yA*xB;

		arie = (1.0 / 2.0) * std::abs(result);

		return arie;
	}
}

bool Triunghi::apartineTriunghi(Punct punctDeComparat)
{
	double arie1 = Triunghi(A1, B1, punctDeComparat).arieTriunghi(true);
	double arie2 = Triunghi(A1, punctDeComparat, C1).arieTriunghi(true);
	double arie3 = Triunghi(punctDeComparat, B1, C1).arieTriunghi(true);


	double arieCompusa = arie1 + arie2 + arie3;
	double arieCompusaRotunjita = round(arieCompusa * 10) / 10;

	double arieRotunjita = round(this->arieTriunghi(false)* 10 / 10);
	
	return arieRotunjita == arieCompusaRotunjita;;
}

bool Triunghi::seSuprapune(std::vector<Triunghi> triunghiVector)
{
	//se verifica coliziunea dintre triunghiul curent si oricare dintre celelalte triunghiuri
	for (auto &it : triunghiVector) {
		if (it.A1 != this->A1 && it.B1 != this->B1 && it.C1 != this->C1) {
			if (this->apartineTriunghi(it.A1))
				return true;
			if (this->apartineTriunghi(it.B1))
				return true;
			if (this->apartineTriunghi(it.C1))
				return true;
		}
	}

	return false;
}

float Triunghi::getXMaxim()
{
	float xMax = this->A.getX();

	if (xMax < this->B.getX())
		xMax = this->B.getX();

	if (xMax < this->C.getX())
		xMax = this->C.getX();

	return xMax;
}

float Triunghi::getYMaxim()
{
	float yMax = this->A.getY();

	if (yMax < this->B.getY())
		yMax = this->B.getY();

	if (yMax < this->C.getY())
		yMax = this->C.getY();

	return yMax;

}

float Triunghi::getXMinim()
{
	float xMin = this->A.getX();

	if (xMin > this->B.getX())
		xMin = this->B.getX();

	if (xMin > this->C.getX())
		xMin = this->C.getX();

	return xMin;
}

float Triunghi::getYMinim()
{
	float yMin = this->A.getY();

		if (yMin > this->B.getY())
			yMin = this->B.getY();

		if (yMin > this->C.getY())
			yMin = this->C.getY();

		return yMin;
}

void Triunghi::desen()
{
	glPointSize(15);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0, 0.0, 0.0);
	glVertex2f(A.getX(), A.getY());
	glVertex2f(B.getX(), B.getY());
	glVertex2f(C.getX(), C.getY());
	glEnd();
}

void Triunghi::transformari(bool suprapus)
{
	glPushMatrix();
	glTranslated(this->x, this->y, 0);
	if (suprapus) {
		glScalef(1.0, 1.5, 0);
	}
	this->desen();
	glPopMatrix();

	this->updatePozitie();
}

void Triunghi::updateFactoriTranslatare()
{
	double alphaCopy = alpha;
	double betaCopy = beta;

	this->x = this->x + alpha;
	if (x > constante::XMAXWINDOW - this->getXMaxim()) 
		alpha = -alphaCopy;
	else if (x < constante::XMINWINDOW - this->getXMinim())  //punct extremitate stanga triunghi
		alpha = -alphaCopy;

	y = y + beta;
	if (y > constante::YMAXWINDOW - this->getYMaxim())
		beta = -betaCopy;
	else if (y < constante::YMINWINDOW - this->getYMinim())
		beta = -betaCopy;
	
}

void Triunghi::updatePozitie()
{
	this->A1.setX(A.getX() + this->x);
	this->B1.setX(B.getX() + this->x);
	this->C1.setX(C.getX() + this->x);

	this->A1.setY(A.getY() + this->y);
	this->B1.setY(B.getY() + this->y);
	this->C1.setY(C.getY() + this->y);
}
