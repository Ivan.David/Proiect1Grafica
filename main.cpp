#include <string>
#include <functional>
#include <vector>

#include "Triunghi.h"
#include "constante.cpp"
#include "Textura2D.h"

#include <IL/il.h>
#include <GL/freeglut.h>


namespace {
	static GLdouble x = 0.0;
	static GLdouble y = 0.0;

	static GLdouble alpha = 0.1;
	static GLdouble beta = 0.1;
}

Triunghi triunghi = Triunghi(Punct(20, 30), Punct(60, 30), Punct(40, 60));
Triunghi triunghi1 = Triunghi(Punct(60, 30), Punct(100, 30), Punct(80, 60));
Triunghi triunghi2 = Triunghi(Punct(100, 30), Punct(140, 30), Punct(120, 60));
Triunghi triunghi3 = Triunghi(Punct(40, 65), Punct(80, 65), Punct(60, 95));
Triunghi triunghi4 = Triunghi(Punct(80, 65), Punct(120, 65), Punct(100, 95));
Triunghi triunghi5 = Triunghi(Punct(60, 100), Punct(100, 100), Punct(80, 130));

std::vector<Triunghi> vector = { triunghi, triunghi1, triunghi2, triunghi3, triunghi4, triunghi5 };
std::vector<Triunghi*> vectorPasari;


int miscaD = 0;

void glPrintf(int x, int y, std::string string) {
	for (int i = 0; i < string.length(); i++) {
		glColor3f(0.0, 0.0, 0.0);
		glRasterPos2i(x + 12 * i, y);  //unde mut cursorul curent pentru a desena bitmap char ul respectiv
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[i]);
	}

}

void init(void)  // initializare fereastra de vizualizare
{
	glClearColor(1.0, 1.0, 1.0, 0.0); // precizeaza culoarea de fond a ferestrei de vizualizare

	glMatrixMode(GL_MODELVIEW);  // se precizeaza este vorba de o reprezentare 2D, realizata prin proiectie ortogonala
	gluOrtho2D(constante::XMINWINDOW, constante::XMAXWINDOW, constante::YMINWINDOW, constante::YMAXWINDOW); // sunt indicate coordonatele extreme ale ferestrei de vizualizare
}

void desen(void) // procedura desenare  
{
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);

	if (miscaD == 1) {
        GLuint inactiveGuntex = Textura2D::creeazaTextura("inactiveGun.png");

        glBindTexture(GL_TEXTURE_2D, inactiveGuntex);
        glBegin(GL_QUADS); //se creeaza un dreptunghi pe care se "lipeste" textura
		glColor3f(1.0, 1.0, 1.0);
        glTexCoord2i(1, 1); glVertex2i(600,   0);
        glTexCoord2i(1, 0); glVertex2i(600,   200);
        glTexCoord2i(0, 0); glVertex2i(800, 200);
        glTexCoord2i(0, 1); glVertex2i(800, 0);
        glEnd();

		glPushMatrix();
		glTranslated(x, y, 0);
        for (auto &i : vectorPasari) {
            i->desen();
        }
		glPopMatrix();

	}
	else if (miscaD == -1) {

		GLuint inactiveGuntex = Textura2D::creeazaTextura("activeGun.png");

		glBindTexture(GL_TEXTURE_2D, inactiveGuntex);
		glBegin(GL_QUADS); //se creeaza un dreptunghi pe care se "lipeste" textura
		glColor3f(1.0, 1.0, 1.0);
		glTexCoord2i(1, 1); glVertex2i(600,   0);
		glTexCoord2i(1, 0); glVertex2i(600,   200);
		glTexCoord2i(0, 0); glVertex2i(800, 200);
		glTexCoord2i(0, 1); glVertex2i(800, 0);
		glEnd();
		
		std::vector<Triunghi> vectorTriunghi = { triunghi, triunghi1, triunghi2, triunghi3, triunghi4, triunghi5 };
        //un vector in care pozitiile triunghiurilor sunt updatate in timp real;
        //nu am dorit sa pierd pozitia initiala

        for (auto &i : vectorPasari){
            if (i->seSuprapune(vectorTriunghi)){
                i->transformari(true);
            }
            else{
                i->transformari(false);
            }
        }

		for (auto &it : vectorTriunghi) {
			it.updatePozitie();
		}
	}
	else {
		std::string string1 = "La click-dreapta se poate observa";
		std::string string2 = "zborul in stol";
		std::string string3 = "La click-stanga se produce un zgomot,";
		std::string string4 = "iar pasarile se sperie si zboara aleator";

		glPrintf(300, 220, string3);
		glPrintf(300, 200, string4);
		glPrintf(300, 240, string2);
		glPrintf(300, 260, string1);

		for (auto &it : vector) {
			it.desen();
		}
	}
	
	glutSwapBuffers();
	glFlush(); // proceseaza procedurile OpenGL cat mai rapid
}

void miscad(void)
{
	miscaD = 1;
	x = x + alpha;
	if (x > constante::XMAXWINDOW - triunghi2.getXMaxim()) 
		alpha = -0.05;
	else if (x < constante::XMINWINDOW - triunghi.getXMinim())  //punct extremitate stanga triunghi
		alpha = 0.05;

	if (y > constante::YMAXWINDOW - triunghi5.getYMaxim())
		beta = -0.05;
	else if (y < constante::YMINWINDOW - triunghi.getYMinim())
		beta = 0.05;
	y = y + beta;

	glutPostRedisplay();
}

void miscas(void)
{
	miscaD = -1;

    for (auto &i : vectorPasari){
        i->updateFactoriTranslatare();
    }

	glutPostRedisplay();
}
void mouse(int button, int state, int x, int y)
{
	switch (button) {
	case GLUT_LEFT_BUTTON:
		glutIdleFunc(miscas);
		break;
	case GLUT_RIGHT_BUTTON: 
		glutIdleFunc(miscad);
		break;
	default:
		break;
	}
}
int main(int argc, char** argv)
{
    vectorPasari.push_back(&triunghi);
    vectorPasari.push_back(&triunghi1);
    vectorPasari.push_back(&triunghi2);
    vectorPasari.push_back(&triunghi3);
    vectorPasari.push_back(&triunghi4);
    vectorPasari.push_back(&triunghi5);

	glutInit(&argc, argv); // initializare GLUT
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); // se utilizeaza un singur buffer | modul de colorare RedGreenBlue (= default)
	glutInitWindowPosition(100, 100); // pozitia initiala a ferestrei de vizualizare (in coordonate ecran)
	glutInitWindowSize(constante::XMAXWINDOW, constante::YMAXWINDOW); // dimensiunile ferestrei 
	glutCreateWindow("Stol de pasari"); // creeaza fereastra, indicand numele ferestrei de vizualizare - apare in partea superioara

    ilInit(); //initializare devIL
	init(); // executa procedura de initializare
	glClear(GL_COLOR_BUFFER_BIT); // reprezentare si colorare fereastra de vizualizare
	glutDisplayFunc(desen); // procedura desen este invocata ori de cate ori este nevoie
	glutMouseFunc(mouse);

	glutMainLoop(); // ultima instructiune a programului, asteapta (eventuale) noi date de intrare

    return 0;
}
