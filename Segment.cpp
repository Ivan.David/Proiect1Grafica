#include "Segment.h"

#include <iostream>

bool Segment::seIntersecteaza(Segment altSegment)
{
	if (ecuatiaDreptei(altSegment.baza)*ecuatiaDreptei(altSegment.varf) < 0.0f) {
		return true;
	}

	return false;
}

Punct Segment::getPunctIntersectie(Segment altSegment)
{ 
	std::map<std::string, float> parameters;

	parameters["alpha1"] = this->alpha;
	parameters["beta1"] = this->beta;
	parameters["gamma1"] = this->gamma;

	parameters["alpha2"] = altSegment.alpha;
	parameters["beta2"] = altSegment.beta;
	parameters["gamma2"] = altSegment.gamma;


	if (seIntersecteaza(altSegment)) {
		Punct punctIntersectie;

		punctIntersectie.setX(-(parameters["beta1"] * (parameters["alpha2"] * parameters["gamma1"] - parameters["alpha1"] * parameters["gamma2"])
			/ (parameters["alpha1"] * parameters["beta2"] - parameters["alpha2"] * parameters["beta1"]) + parameters["gamma1"]) / parameters["alpha1"]);
		punctIntersectie.setY((parameters["alpha2"] * parameters["gamma1"] - parameters["alpha1"] * parameters["gamma2"]) /
			(parameters["alpha1"] * parameters["beta2"] - parameters["alpha2"] * parameters["beta1"]));

		return punctIntersectie;
	}

	Punct punctIntersectie;
	std::cout << "Segmentele nu se intersecteaza";
	
	punctIntersectie.setX(2);
	return punctIntersectie;
}


std::map<std::string, float> Segment::getCoordVarfuri()
{
	std::map <std::string, float> mapCoordonate;

	mapCoordonate["aX"] = this->baza.getX();
	mapCoordonate["aY"] = this->baza.getY();
	mapCoordonate["bX"] = this->varf.getX();
	mapCoordonate["bY"] = this->varf.getY();
	
	return mapCoordonate;
}



float Segment::ecuatiaDreptei(Punct punctDeCalculAlEcuatiei)
//calculeaza valoarea reala a ecuatiei dreptei intr-un anumit punct
{
	float ecuatiaInPunct = alpha*punctDeCalculAlEcuatiei.getX() + beta*punctDeCalculAlEcuatiei.getY() + gamma;
	return ecuatiaInPunct;
}
