#include "Punct.h"

#include <iostream>
#include <limits>

Punct::Punct()
{
	this->x = std::numeric_limits<float>::min();
	this->y = std::numeric_limits<float>::min();
}

Punct::Punct(float x, float y)
{
	this->x = x;
	this->y = y;
}

bool Punct::operator==(const Punct& punct) const {
	//un punct este egal cu altul daca cele doua coordonate (x,y) sunt egale
	return (this->getX() == punct.getX() && this->getY() == punct.getY());

};

bool Punct::operator!=(const Punct& altPunct) const {
	return !(*this == altPunct);
}
