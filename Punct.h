enum class Curbura {concav, convex, nedefinit};

class Punct {
private:
	float x;
	float y;

public:

	int semn;
	Curbura curbura = Curbura::nedefinit;
	bool ePunctPrincipal = true;
	bool eEarPoint = false;

	Punct();
	~Punct() {};
	Punct(float x, float y);

	const float getX() const { return this->x; }
	const float getY() const { return this->y; }
	void setX(float x) { this->x = x; }
	void setY(float y) { this->y = y; }

	bool operator==(const Punct &punct) const;
	bool operator!=(const Punct &punct) const;
};