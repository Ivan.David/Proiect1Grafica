//
// Created by David Ivan on 4/26/17.
//

#ifndef GRAFICA_STOL_TEXTURA2D_H
#define GRAFICA_STOL_TEXTURA2D_H


#include <GL/gl.h>
#include <string>

class Textura2D {
public:
    static GLuint creeazaTextura(std::string path);
    static int incarcaImagine(std::string filename);
private:

};


#endif //GRAFICA_STOL_TEXTURA2D_H
