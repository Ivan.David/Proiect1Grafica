namespace constante {
	//pentru simplitate, XMAXWINDOW si YMAXWINDOW sunt folosite drept coordonate extreme ale ferestrei,
	//dar si ca dimensiuni efective ale ferestrei

	double const XMINWINDOW = 0.0;
	double const XMAXWINDOW = 800.0;
	double const YMINWINDOW = 0.0;
	double const YMAXWINDOW = 600.0;
}