#include "Punct.h"
#include <map>


class Segment {
private:
	
	float alpha;
	float beta;
	float gamma;

	float ecuatiaDreptei(Punct a);

protected:
	Punct baza, varf;

public:
	bool intersecteazaSegm = false;

	Segment() {};
	~Segment() {};

	Segment(Punct a, Punct b) {
		this->baza = a;
		this->varf = b;

		this->alpha = this->varf.getY() - this->baza.getY();
		this->beta = this->baza.getX() - this->varf.getX();
		this->gamma = this->varf.getX()*this->baza.getY() - this->baza.getX()*this->varf.getY();
	}

	bool seIntersecteaza(Segment altSegment);
	Punct getPunctIntersectie(Segment altSegment);
	
	std::map<std::string, float> getCoordVarfuri();

	int getZVarfCuSegment(Segment SegmentCuCareFormeazaVarf);
	
};