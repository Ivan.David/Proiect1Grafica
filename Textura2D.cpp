//
// Created by David Ivan on 4/26/17.
//

#include <IL/il.h>
#include <iostream>
#include "Textura2D.h"

int Textura2D::incarcaImagine(std::string filename)
{
    ILboolean success;
    ILuint image;

    ilGenImages(1, &image); // genereaza un nume imaginii, pe care il stocheaza in image
    ilBindImage(image); // atribuie un nume imaginii
    success = ilLoadImage(filename.c_str()); // incarca imaginea descrisa de filename

    if (success) // != 0 => operatiile de mai sus s-au efectuat cu succes
    {
        // converteste imaginea RGBA in intregi
        success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

        if (!success)
        {
            return -1;
        }
    }
    else
        return -1;

    return image;
}


GLuint Textura2D::creeazaTextura(std::string path) {
    GLuint texid;
    int    image;

    ilInit();

    // incarca imaginea cu devIL
    image = incarcaImagine(path);
    if ( image == -1 )
    {
        std::cout << "DevIL nu poate incarca imaginea de la" << path;
        return -1;
    }

    /* OpenGL texture binding of the image loaded by DevIL  */
    glGenTextures(1, &texid); // genereaza un nume pentru textura
    glBindTexture(GL_TEXTURE_2D, texid); //atribuie un nume texturii
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //interpolare liniara pentru marire
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //interpolare liniara pentru micsorare
    glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
                 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData());

    return texid;
}

